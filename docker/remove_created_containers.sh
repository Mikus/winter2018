#!/bin/bash
#  file: remove_created_containers.sh
# usage: sudo ./remove_created_containers.sh

(cd "$( dirname "${BASH_SOURCE[0]}" )" && docker-compose stop && docker-compose rm -f)

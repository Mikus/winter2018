FROM openjdk:jre-alpine
MAINTAINER EPM-LSTR-BCN
RUN mkdir -p /usr/beacon/logs/
COPY dist/beacons-cloud-gateway.jar /usr/beacon/

RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

WORKDIR /usr/beacon/
EXPOSE 10020
CMD ["dockerize", "-wait", "tcp://localhost:10005", "-timeout", "1000s",  "java", "-Dspring.profiles.active=DEV", "-DlogsPath=/usr/beacon/logs", "-DlogFileName=cloud-gateway.log", "-jar", "/usr/beacon/beacons-cloud-gateway.jar"]
#CMD ["java", "-Dspring.profiles.active=DEV", "-DlogsPath=/usr/beacon/logs", "-DlogFileName=cloud-gateway.log", "-jar", "/usr/beacon/beacons-cloud-gateway.jar"]


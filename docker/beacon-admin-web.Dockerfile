FROM node
MAINTAINER EPM-LSTR-BCN
RUN mkdir -p /usr/beacon/
COPY beacon-admin-web/ /usr/beacon/
WORKDIR /usr/beacon/beacon-admin-web
RUN npm install
ENV PORT 8080
CMD [ "npm", "start"]

FROM openjdk:jre-alpine
MAINTAINER EPM-LSTR-BCN
RUN mkdir -p /usr/beacon/logs/


RUN apk add --no-cache openssl

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz


COPY dist/beacons-cloud-uaa-service.jar /usr/beacon/
WORKDIR /usr/beacon/
EXPOSE 10035
CMD ["dockerize", "-wait", "tcp://localhost:10020", "-timeout", "1000s", "java","-Dspring.profiles.active=DEV", "-DlogsPath=/usr/beacon/logs", "-DlogFileName=uaa-service.log", "-jar", "/usr/beacon/beacons-cloud-uaa-service.jar"]


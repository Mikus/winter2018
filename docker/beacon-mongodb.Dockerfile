FROM mongo:latest
MAINTAINER EPM-LSTR-BCN
ENV MONGO_DATA_DIR=/data/db
ENV MONGO_LOG_DIR=/dev/null
COPY createEnvScript.js /data/db/createEnvScript.js
EXPOSE 27017
WORKDIR /data/db/
CMD ["mongo", "createEnvScript.js"]
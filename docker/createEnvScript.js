var dbs = db.getMongo().getDBNames()
for(var i in dbs){
    db = db.getMongo().getDB( dbs[i] );
    print( "dropping db " + db.getName() );
    db.dropUser("root");
    db.dropDatabase();
}

var dbList = ["building", "filedb", "sequence", "settings", "user"];
for(var j in dbList){
    db = db.getMongo().getDB( dbList[j] );
    db.collectionOne.insertOne({x:1});
    db.collectionOne.deleteOne({x:1});
    db.createUser({user: "root", pwd: "root", roles: [{role: "readWrite", db: dbList[j] }]});
    print( "Created db ", dbList[j] );
}


FROM openjdk:8-jre-slim
MAINTAINER EPM-LSTR-BCN
RUN mkdir -p /usr/beacon/logs/
COPY dist/beacons-cloud-config-service.jar /usr/beacon/
WORKDIR /usr/beacon/
EXPOSE 10100
CMD ["java", "-Dspring.profiles.active=DEV", "-DlogsPath=/usr/beacon/logs", "-DlogFileName=cloud-config-service.log", "-jar", "/usr/beacon/beacons-cloud-config-service.jar"]              
